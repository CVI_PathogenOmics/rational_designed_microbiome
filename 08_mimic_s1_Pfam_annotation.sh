#=============================================================================================
#author			Neeraj Kumar et al., 2021; https://github.com/ClavelLab/MiMiC
#date			02-2021
#adjusted by		Paul Stege
#script			Metagenome pipeline.
#software		Doug Hyatt et al., 2010; https://github.com/hyattpd/Prodigal
#			Simon C Potter  et al., 2018 https://github.com/EddyRivasLab/hmmer
# 			Jaina Mistry et al., 2021;  http://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/
#=============================================================================================
# usage
# bash 08_mimic_s1_Pfam_annotation.sh -h
# bash 08_mimic_s1_Pfam_annotation.sh -d PathWorkDir -i InputFolder -p PathToPfamDatabase -t optionforProdigal(meta or single) -s sample 

#=============================================================================================
# settings
#!/bin/bash

	if [ "$1" == "-h" ]; then
		echo -e "\n\n"
		echo -e "********  Usage ***********************\n\n"
		echo -e "Usage: bash s1_contig_to_Pfam.sh -d PathWorkDir -i InputFolder -p PathToPfamDatabase -t optionforProdigal(meta or single) -s sample\n\n"
		echo -e "[options]\n"
		echo -e "	-d : this is the path for genome or metagenome assembly folder to be annotated e.g. /home/project/"
		echo -e "	-i : this is the name of the input folder for genome or metagenome assembly to be annotated"
		echo -e "	-p : this is the folder for hmmpress pfam database"
		echo -e "	-t : this provides an option to prodigal to be in metagenomic mode or single genome mode mode\n\n"
		echo -e "	-s : this provides the sample for processing \n\n"
		exit 0
	fi

	while getopts i:p:d:t:s: flag
		do
		    case "${flag}" in
		        d) indir=${OPTARG};;
		        i) input_fna=${OPTARG};;
		        p) PfamPath=${OPTARG};;
		        t) Type=${OPTARG};;
		        s) sample=${OPTARG};;
		    esac
		done


#================ setting directories =====================================

	#Prodigal
	ProdigalPath=prodigal
	
	#hmmscan
	hmmscanPath=hmmscan

	mkdir -p $indir/08_mimic_output/output_faa_${Type}
	mkdir -p $indir/08_mimic_output/output_pfam_${Type}
	mkdir -p $indir/08_mimic_output/output_fna_${Type}

#================ run tools =====================================

# running script for each genome or metagenome
	
	# each sample generate two samples R1 and R2. sample names should be the same. only sufix in the file should be different. 
	echo "=================================================================="
	echo �sample ${sample}�
	echo "=================================================================="

#prodigal --> gene prediction

	# build log file
	echo "==================================================================" >>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log
	echo �sample ${sample}� >>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log
	echo "==================================================================" >>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log
	
	# run prodigal
	echo "start prodigal at $(date +%x) $(date +%R)" >>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log
	$ProdigalPath -i $indir/$input_fna/${sample}_*.faa -p $Type \
			-a $indir/08_mimic_output/output_faa_${Type}/${sample}.faa \
			-d $indir/08_mimic_output/output_fna_${Type}/${sample}.fna \
			-c -m -q -f sco

 	# include in log file
	echo "prodigal finished at $(date +%x) $(date +%R)">>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log
	ProteinCount=$(grep -c "^" $indir/08_mimic_output/output_faa_${Type}/${sample}.faa) 
	echo -e "proteins detected: $ProteinCount" >> $indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log 
	echo "starting hmmscan">>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log 

#hmmscan ----> pfam prediction
	
	$hmmscanPath --cpu 2 --acc --noali --cut_ga --tblout  $indir/08_mimic_output/output_pfam_${Type}/${sample}.faa.table_results $PfamPath/Pfam-A_2019.hmm $indir/08_mimic_output/output_faa_${Type}/${sample}.faa
	echo "hmmscan finished at $(date +%x) $(date +%R)" >>$indir/08_mimic_s1_Pfam_annotation_${Type}.sh.log 	

echo -e "processing is finished \n\n" 
#=============================================================================================
# script end
# Paul Stege; paul.stege@wur.nl