#=============================================================================================
#author			Paul Stege
#date			11-2022
#script			Analyse output kraken for abundance and prevalence
#script			Download genomes from NCBI DB
#software		Kai Blin et al., 2021 ; https://github.com/kblin/ncbi-genome-download
#============================================================
# usage
# bash ./07_NCBI_download.sh -d PathWorkDir
#============================================================
# settings
#!/bin/bash

	if [ "$1" == "-h" ]; then
		echo -e "\n\n"
		echo -e "********  Usage ***********************\n\n"
		echo -e "Usage: bash ./07_NCBI_download.sh -d PathWorkDir \n\n"
		echo -e "[options]\n"
		echo -e "	-d : this is the path to the root folder of the project e.g. /home/project/"
		exit 0
	fi

	while getopts d:p:b:k:s: flag
		do
		    case "${flag}" in
		        d) INDIR=${OPTARG};;
		    esac
		done

#============================================================

VSOFT="ncbi-genome-download -v"
mkdir -p ${INDIR}/07_spec_genomes

echo "running $VSOFT" 

while read species; do
echo "=================================================================="
echo ${species}
echo "=================================================================="

  species_entry=$(ncbi-genome-download --dry-run --assembly-levels complete  --refseq-categories representative --genera "${species}" bacteria  2>&1 | sed -n 2p)

  if test -z "$species_entry" 
     then
     	echo "representative sequence is not available, continue to download complete genome"
	species_entry=$(ncbi-genome-download --dry-run --assembly-levels complete --genera "${species}" bacteria  2>&1 | sed -n 2p)
	species_gcid=$(echo -e $species_entry | awk '{print $1}')
	echo "download" $species_gcid

	ncbi-genome-download --assembly-levels complete \
		-F fasta -A "${species_gcid}" bacteria \
		-o ${INDIR}/07_spec_genomes/ \
		--flat-output
  
     else
      	echo "download representative sequence"
	species_entry=$(ncbi-genome-download --dry-run --assembly-levels complete  --refseq-categories representative --genera "${species}" bacteria  2>&1 | sed -n 2p)
	species_gcid=$(echo -e $species_entry | awk '{print $1}')
	echo "download" $species_gcid

	ncbi-genome-download --assembly-levels complete \
		 --refseq-categories representative \
		-F fasta -A "${species_gcid}" bacteria \
		-o ${INDIR}/07_spec_genomes/ \
		--flat-output

  fi

  gunzip ${INDIR}/07_spec_genomes/${species_gcid}*.gz -f
  mv ${INDIR}/07_spec_genomes/${species_gcid}*_genomic.fna ${INDIR}/07_spec_genomes/${species_gcid}_genomic.faa
  echo $species_entry >> ${INDIR}/07_spec_genomes/species_meta.txt


done < ${INDIR}/06_taxa_output/spec_list_core_microbiota.tab

ls ${INDIR}/07_spec_genomes/*faa |sed 's@.*/@@'| tee ${INDIR}/07_spec_genomes/filenames.tab
#=============================================================================================
# script end
# Paul Stege; paul.stege@wur.nl
