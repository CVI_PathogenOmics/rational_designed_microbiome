#=============================================================================================
#author			Paul Stege
#date			11-2022
#script			Annotate genes with KEGG function
#software		Aramaki T. et al., 2019 ; https://github.com/takaram/kofam_scan
#=============================================================================================
# Usage
# bash 12_kofam_pathway.sh -h
# bash 12_kofam_pathway.sh -d PathWorkDir -k KoList -p KoProfile -t optionforProdigal(meta or single) -s sample 
#=============================================================================================
# make options
#!/bin/bash

	if [ "$1" == "-h" ]; then
		echo -e "\n\n"
		echo -e "********  Usage ***********************\n\n"
		echo -e "Usage: bash 12_kofam_pathway.sh -d PathWorkDir -k KoList -p KoProfile -t optionforProdigal(meta or single) -s sample \n\n"
		echo -e "[options]\n"
		echo -e "	-d : this is the path for genome or metagenome assembly folder to be annotated e.g. /home/project/"
		echo -e "	-k : this is the folder for Kofam ko list database"
		echo -e "	-p : this is the folder for Kofam profile database"
		echo -e "	-t : this provides an option to prodigal to be in metagenomic mode or single genome mode mode\n\n"
		echo -e "	-s : this provides the sample for processing \n\n"
		exit 0
	fi

	while getopts d:k:p:t:s: flag
		do
		    case "${flag}" in
		        d) DIR=${OPTARG};;
		        k) KOLIST=${OPTARG};;
		        p) KOPROF=${OPTARG};;
		        t) TYPE=${OPTARG};;
		        s) sample=${OPTARG};;
		    esac
		done

#============================================================

VSOFT=$(conda list | grep exec_annotation|awk {'print $1, $2'})

# run software 
	echo "running $VSOFT"  
	mkdir -p ${DIR}/12_kofam_anno/${TYPE}/tmp_${sample}     # temp dir

	echo "=================================================================="
	echo �sample ${sample}�
	echo "=================================================================="
  
  	exec_annotation ${DIR}/08_mimic_output/output_faa_${TYPE}/${sample} \
		-p ${KOPROF} \
		-k ${KOLIST} \
		-o ${DIR}/12_kofam_anno/${TYPE}/${sample}.kofam.txt \
		--tmp-dir ${DIR}/12_kofam_anno/${TYPE}/tmp_${sample} \
		--cpu 2 -f mapper-one-line

  	echo "sample completed at $(date +%x) $(date +%R)"
	rm -r ${DIR}/12_kofam_anno/${TYPE}/tmp_${sample}  # remove temp
#=============================================================================================
# script end
# Paul Stege; paul.stege@wur.nl