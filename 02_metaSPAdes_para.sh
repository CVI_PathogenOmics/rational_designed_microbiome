#=============================================================================================
#author			Paul Stege
#date			11-2022
#script p1		Trim and filter sequencing reads
#script p2		Annotate reads to estimate the bacterial species
#script p3		Reads are assembled using metaspades
#software		Ole Tange, 2010; https://github.com/martinda/gnu-parallel
#			Brian Bushnell, 2013; https://sourceforge.net/projects/bbmap/
# 			Sergey Nurk  et al., 2017 https://github.com/ablab/spades
#			Derrick E. Wood et all., 2019; https://github.com/DerrickWood/kraken2
#=============================================================================================
# usage
# bash 02_metaSPAdes_para.sh -d PathWorkDir -i InputFolder -p BBmapAdap -b PathToBBmapDB -k PathToKrakenDB -s sample 
#=============================================================================================
# settings
#!/bin/bash

	if [ "$1" == "-h" ]; then
		echo -e "\n\n"
		echo -e "********  Usage ***********************\n\n"
		echo -e "Usage: bash 02_metaSPAdes_para.sh -d PathWorkDir -p BBmapAdap -b PathToBBmapDB -k PathToKrakenDB -s sample \n\n"
		echo -e "[options]\n"
		echo -e "	-d : this is the path for genome or metagenome assembly folder to be annotated e.g. /home/project/"
		echo -e "	-p : this is the path to BBmap adapters e.g. opt/bbmap-38.18/resources/adapters.fa"
		echo -e "	-b : this is the path to BBmap host DNA database e.g. bbmap_DB/bGalGal_broiler_DB"
		echo -e "	-k : this is the path to the KrakenDB e.g. kraken2/kraken_DB_17052021_standard"
		echo -e "	-s : this provides the sample for processing \n\n"
		exit 0
	fi

	while getopts d:p:b:k:s: flag
		do
		    case "${flag}" in
		        d) indir=${OPTARG};;
		        p) bbadp=${OPTARG};;
		        b) bbdb=${OPTARG};;
		        k) krakdb=${OPTARG};;
		        s) sample=${OPTARG};;
		    esac
		done

#============================================================

THREADS=8
MINLENGTH=30 #readlength minimum after trimming
TRIMQUAL=20  #which q for trimming
VSOFT1=$(conda list | grep bbmap|awk {'print $1, $2'})
VSOFT2="metaspades.py -v"
VSOFT3=$(conda list | grep kraken2|awk {'print $1, $2'})

mkdir -p ${indir}/02_trim_concat_raw
mkdir -p ${indir}/03_filt_trim_concat_raw
mkdir -p ${indir}/04_metaspades_contig
mkdir -p ${indir}/05_kraken_anno
mkdir -p ${indir}/09_temp_log_parts

echo "running $VSOFT1,$VSOFT2 and $VSOFT3"  

echo "=================================================================="
echo �sample ${sample}�
echo "=================================================================="

# logfile
	echo "Processing sample..." > ${indir}/09_temp_log_parts/reads_per_sample_p_${sample}.txt

# run bbmap-bbduk trimming
	echo "===> Length filtering below 30 bases. Keeping PAIRS!"

	bbduk.sh -Xmx30g -Xms30g \
			 in1=${indir}/01_concat_raw_per_sample/${sample}_1.fastq.gz \
			 in2=${indir}/01_concat_raw_per_sample/${sample}_2.fastq.gz \
			 out1=${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R1.fastq.gz \
			 out2=${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R3.fastq.gz \
			 ref=${bbadp}\
			 k=17 mink=6 ktrim=r ftm=5 qtrim=rl threads=$THREADS \
			 trimq=$TRIMQUAL \
			 minlen=$MINLENGTH

	# count reads
	LOG_SAMPLE=${sample}
	LOG_RREADS=$(($(zcat ${indir}/01_concat_raw_per_sample/${sample}_1.fastq.gz|wc -l)/2|bc))
	LOG_TREADS=$(($(zcat ${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R3.fastq.gz|wc -l)/2|bc))

# remove host DNA with BBmap, continue with unmapped reads
	bbmap.sh -Xmx30g -Xms30g \
	 	in1=${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R1.fastq.gz \
	 	in2=${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R3.fastq.gz \
		outu1=${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz \
 		outu2=${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz \
		path=${bbdb} \
		t=$THREADS fast=t

	# remove temp files
	rm ${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R1.fastq.gz
	rm ${indir}/02_trim_concat_raw/${sample}_adapterQualClip.minlen30_concat_R3.fastq.gz

	# count filtered reads
	LOG_FREADS=$(($(zcat ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz|wc -l)/2|bc))

# run Kraken2 RefSeq nucleotide DB
	echo "Run Kraken2" 
	kraken2 --paired ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz \
		--output ${indir}/05_kraken_anno/${sample}_kraken.out \
		--report ${indir}/05_kraken_anno/${sample}.kreport \
		-db ${krakdb} \
		--threads $THREADS --gzip-compressed --use-names --confidence 0.3

	# remove temp files
	rm ${indir}/05_kraken_anno/${sample}_kraken.out

	# extract annotated reads
	KRAK_READS=$(awk 'FNR == 2 {print $2}' ${indir}/05_kraken_anno/${sample}.kreport) 
	KRAK_UNREADS=$(awk 'FNR == 1 {print $2}' ${indir}/05_kraken_anno/${sample}.kreport)


# if trimming was succesfull, continue to assembly

  if test -f "${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz"; then

  	# remove downloaded genome files
	rm ${indir}/01_concat_raw_per_sample/${sample}_1.fastq.gz
	rm ${indir}/01_concat_raw_per_sample/${sample}_2.fastq.gz

        # run metaspades assembly 
	echo "Run metaspades" 
	metaspades.py -1 ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz \
 			-2 ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz \
 			-o ${indir}/04_metaspades_contig/${sample}\
			-t $THREADS -m 250
	
  else 
	echo "trimming failed" > ${indir}/09_temp_log_parts/reads_per_sample_p_${sample}.txt

  fi

# if assembly was succesfull, continue to final contig steps 

  if test -f "${indir}/04_metaspades_contig/${sample}/contigs.fasta"; then

	# move files and remove temp files
	mv ${indir}/04_metaspades_contig/${sample}/contigs.fasta ${indir}/04_metaspades_contig/${sample}_contigs.fasta
	mv ${indir}/04_metaspades_contig/${sample}/spades.log ${indir}/04_metaspades_contig/${sample}_spades.log
	rm -r ${indir}/04_metaspades_contig/${sample}

	# count contigs
	LOG_MCON=$(grep -c ">" ${indir}/04_metaspades_contig/${sample}_contigs.fasta)

	# move trimmed reads to archive (remove this step when upload github)
	mv ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz /mnt/lely_archive/stege030/KB_rational/tom_96x/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R1.fastq.gz
	mv ${indir}/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz /mnt/lely_archive/stege030/KB_rational/tom_96x/03_filt_trim_concat_raw/${sample}_filt_trim_concat_R3.fastq.gz

	# contig size cutoff
	echo "Run BBmap"
	reformat.sh in=${indir}/04_metaspades_contig/${sample}_contigs.fasta \
			out=${indir}/04_metaspades_contig/${sample}_filtcontigs.faa \
			minlength=500 threads=$THREADS

	# count filtered contigs
	LOG_FCON=$(grep -c ">" ${indir}/04_metaspades_contig/${sample}_filtcontigs.faa)

	# remove temp files
	rm ${indir}/04_metaspades_contig/${sample}_contigs.fasta

	# export sample logfile
	printf "%-20s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s\n" "$LOG_SAMPLE" "$LOG_RREADS" "$LOG_TREADS" "$LOG_FREADS" "$KRAK_READS" "$KRAK_UNREADS" "$LOG_MCON" "$LOG_FCON" > ${indir}/09_temp_log_parts/reads_per_sample_p_${sample}.txt
  
  else 
	echo "contig assembly failed" > ${indir}/09_temp_log_parts/reads_per_sample_p_${sample}.txt

  fi

  #=============================================================================================
  # script end
  # Paul Stege; paul.stege@wur.nl
