#=============================================================================================
#author			Paul Stege
#date			11-2022
#script			Convert kraken files into single biom file
#script p2		Annotate reads to estimate the bacterial species
#script p3		Reads are assembled using metaspades
#software		S.M. Dabdoub, 2016; https://github.com/smdabdoub/kraken-biom
#=============================================================================================
# usage
# bash ./05_kraken_output.sh -d PathWorkDir
#=============================================================================================
# settings
#!/bin/bash

	if [ "$1" == "-h" ]; then
		echo -e "\n\n"
		echo -e "********  Usage ***********************\n\n"
		echo -e "Usage: bash ./05_kraken_output.sh -d PathWorkDir \n\n"
		echo -e "[options]\n"
		echo -e "	-d : this is the path to the root folder of the project e.g. /home/project/"
		exit 0
	fi

	while getopts d:p:b:k:s: flag
		do
		    case "${flag}" in
		        d) INDIR=${OPTARG};;
		    esac
		done

#============================================================
# Shape Kraken file
DATE=`date +"%m%Y"`
VSOFT=$(conda list | grep kraken-biom|awk {'print $1, $2'})

echo "running $VSOFT"  
kraken-biom ${INDIR}/05_kraken_anno/*.kreport --fmt json -o ${INDIR}/05_kraken_anno/kraken_assemble_summ_${DATE}.biom

#============================================================
# Shape log file
echo "merge logfile previous steps"
printf "%-20s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s | %-20s\n" "sample_ID" "raw_reads" "trimmed_reads" "filtered_reads" "krak_readpairs" "krak_unreadpairs" "nr_contigs" "nr_filt_contigs"> $INDIR/reads_per_sample.txt 
cat ${INDIR}/09_temp_log_parts/reads_per_sample_* >> $INDIR/reads_per_sample.txt
#=============================================================================================
# script end
# Paul Stege; paul.stege@wur.nl
